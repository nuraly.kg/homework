// import 'package:dart_application_1/dart_application_1.dart' as dart_application_1;

void main(List<String> arguments) {
//  1-задание!
  List<int> a = [1, 7, 12, 3, 56, 2, 87, 34, 54];
  print('Первый: ${a.first} Пятый: ${a[4]} Последний: ${a.last}');

//  2-задание!
  List<int> b = [3, 12, 43, 1, 25, 6, 5, 7];
  List<int> c = [55, 11, 23, 56, 78, 1, 9];
  // Я нашел 3 способа
  print([...b, ...c]);
  print(b + c);
  b.addAll(c);
  print(b);

  //  3-задание!
  List ar = [
    'a',
    'd',
    'F',
    'l',
    'u',
    't',
    't',
    'e',
    'R',
    'y',
    '3',
    'b',
    'h',
    'j'
  ];
  print(ar.sublist(2, 9));

  //  4-задание!
  List rr = [1, 2, 3, 4, 5, 6, 7];
  print(rr.contains(3));
  print(rr.first);
  print(rr.last);
  print(rr.length);

  //  5-задание!
  List z = [601, 123, 2, 'dart', 45, 95, "dart24", 1];
  print(z.contains('dart'));
  print(z.contains(951));
  print(z.contains('dart') && z.contains(951));
//  6-задание!
  List q = ['post', 1, 0, 'flutter'];
  String myDart = 'Flutter';
  String p = myDart.toLowerCase();
  print(q.contains(p));

  // print(q.runtimeType);

  //  7-задание!
  List ilearn = ['I', 'Started', 'Learn', 'Flutter', 'Since', 'April'];
  String myFlutter = '';
  myFlutter = ilearn.join('*');
  print(myFlutter);

  //  7-задание!
  List w = [1, 9, 3, 195, 202, 2, 5, 7, 9, 10, 3, 15, 0, 11];

  w.sort((a, b) => a.compareTo(b));
  print(w);
  // или можем обратно
  w.sort(((a, b) => b.compareTo(a)));
  print(w);
}
